﻿ using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using movieAPI.Models;

namespace movieAPI.Services
{
    public class UserService
    {
        private ApplicationDbContext _context;
        private IConfiguration _configuration;

        public UserService(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<Response> Create(User user)
        {
            try
            {
                Response resp = new();
                bool duplicateEmail = await new AuthService(_context, _configuration).checkEmail(user.Email);
                if (!duplicateEmail)
                {
                    user.CreatedAt = DateTime.Now;
                    byte[] passwordBytes = Helpers.EncryptStringToBytes_Aes(user.Password, Encoding.ASCII.GetBytes(_configuration["Aes:Key"]), Encoding.ASCII.GetBytes(_configuration["Aes:Iv"]));
                    var hexString = BitConverter.ToString(passwordBytes);
                    user.Password = hexString.Replace("-", "");
                    var result = _context.Users.Add(user);
                    await _context.SaveChangesAsync();
                    if (result != null) {
                        resp.statusCode = 200;
                        resp.statusMessage = "Successfully created";
                    } 
                    else resp.statusCode = 404;
                }
                else
                {
                    resp.statusMessage = "This email already registered";
                    resp.statusCode = 409;
                }
                

                return resp;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


    }
}
