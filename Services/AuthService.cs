﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using movieAPI.Models;

namespace movieAPI.Services
{
    public class AuthService
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;

        public AuthService(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<Response> login(Login login)
        {
            try
            {
                Response resp = new();
                
                User user =  await _context.Users.Where(u => u.Email == login.Email).FirstOrDefaultAsync();
                 if (user != null)
                 {
                    byte[] passwordBytes = Helpers.StringToByteArray(user.Password);
                    string decryptedPassword = Helpers.DecryptStringFromBytes_Aes(passwordBytes, Encoding.ASCII.GetBytes(_configuration["Aes:Key"]), Encoding.ASCII.GetBytes(_configuration["Aes:Iv"]));
                    if(login.Password == decryptedPassword)
                    {
                        resp.statusCode = 200;
                        resp.statusMessage = "Successfully";
                        resp.data = new 
                        {
                            email = user.Email,
                            firstName = user.FirstName,
                            lastName = user.LastName,
                            createdDate = user.CreatedAt,
                            token = Helpers.GenerateJWTToken(user, _configuration)

                    };
                    }
                    else
                    {
                        resp.statusCode = 403;
                        resp.statusMessage = "Password is wrong";
                    }
                    
                }

                else
                {
                    resp.statusCode = 404;
                    resp.statusMessage = "Email couldn't find";
                }

                return resp;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> checkEmail(string email)
        {
            var data = await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
            if (data != null) return true;
            return false;
        }
    }
}
