﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using movieAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace movieAPI.Services
{
    public class MovieService
    {
        private ApplicationDbContext _context;
        private IConfiguration _configuration;

        public MovieService(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<Response> Favourite(int movieId, string token)
        {
            try
            {
                Response resp = new();
                FavouriteMovie movie = new FavouriteMovie();
                movie.MovieId = movieId;
                movie.UserId = Convert.ToInt32(Helpers.GetClaimsFromToken(token).FirstOrDefault(c => c.Type == "Id").Value);
                movie.CreatedAt = DateTime.Now;
                FavouriteMovie fav = _context.FavouriteMovies.Where(m => m.MovieId == movieId && m.UserId == movie.UserId).FirstOrDefault();
                EntityEntry result;
                if (fav != null)
                {
                   result = _context.FavouriteMovies.Remove(fav);
                }
                else  result = _context.FavouriteMovies.Add(movie);
                await _context.SaveChangesAsync();

                if(result != null)
                {
                    resp.statusCode = 200;
                    resp.statusMessage = "Movie has been successfully added to your favourites";
                }
                else
                {
                    resp.statusCode = 404;
                }

                return resp;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<Response> GetFavouriteMovies(string token)
        {
            try
            {
                Response resp = new();
                int userId = Convert.ToInt32(Helpers.GetClaimsFromToken(token).FirstOrDefault(c => c.Type == "Id").Value);
                List<Movie> arr = new();
                List<int> favouriteIds = _context.FavouriteMovies.Where(m => m.UserId == userId).Select(m => m.MovieId).ToList();


                var parameters = new Dictionary<string, string>() { { "api_key", _configuration["MovieDB:ApiKey"] } };

                foreach (int id in favouriteIds)
                {
                    JObject jobj = await Helpers.SendRequest(_configuration["MovieDB:Url"], RequestTypes.GET, $"/movie/{id}", parameters, null);
                    Movie m = jobj.ToObject<Movie>();
                    arr.Add(m);
                }

                List<_Movie> _arr = Helpers.MovieFilter(arr).ToList();
                foreach(_Movie _m in _arr)
                {
                    _m.isFavourite = true;
                }

                if (favouriteIds.Count > 0)
                {
                    resp.statusCode = 200;
                    resp.data = _arr;
                }
                else resp.statusCode = 404;

                return resp;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<Response> Explore(int page, string token)
        {   
            try
            {
                Response resp = new();
                
                float pageNumb = (((Convert.ToUInt16(page) * 6) - 6)  / 20) + 1;

                var parameters = new Dictionary<string, string>() { { "api_key", _configuration["MovieDB:ApiKey"] }, { "page", pageNumb.ToString() } };

                JObject jobj = await Helpers.SendRequest(_configuration["MovieDB:Url"], RequestTypes.GET, "/discover/movie", parameters, null);

                Movie[] arr = jobj.SelectToken("results").ToObject<Movie[]>();

                int pageMod = (Convert.ToUInt16(page) * 6) % 20;

                int range = pageMod;
                if (pageMod < 6 && pageMod > 0)
                {
                    range = 20 + pageMod;
                    parameters["page"] = (pageNumb + 1).ToString();
                    JObject nJobj = await Helpers.SendRequest(_configuration["MovieDB:Url"], RequestTypes.GET, "/discover/movie", parameters, null);
                    arr = arr.Concat(nJobj.SelectToken("results").ToObject<Movie[]>()).ToArray();

                }
                range = range != 0 ? range - 6 : range;



                List<_Movie> _arr = Helpers.MovieFilter(arr.ToList().GetRange(range, 6)).ToList();
                List<_Movie> favourites = (List<_Movie>)((await GetFavouriteMovies(token))?.data);

                if(favourites != null && favourites.Count > 0)
                {
                    foreach (_Movie fav in favourites)
                    {
                        foreach (_Movie movie in _arr)
                        {
                            if (movie.id == fav.id) movie.isFavourite = true;
                        }
                    }

                }
                if (arr.Length > 0)
                {
                    resp.statusCode = 200;
                    resp.data = _arr;
                }
                else resp.statusCode = 404;
                return resp;
            }
            catch (Exception ex)
            {
                throw;
            }
                
        }
        public async Task<Response> Search(string query, string token)
        {
            try
            {
                Response resp = new();

                var parameters = new Dictionary<string, string>() { { "api_key", _configuration["MovieDB:ApiKey"] }, { "query", query } };
                JObject jobj = await Helpers.SendRequest(_configuration["MovieDB:Url"], RequestTypes.GET, "/search/movie", parameters, null);



                Movie[] arr = jobj.SelectToken("results").ToObject<Movie[]>();
                List<_Movie> _arr = Helpers.MovieFilter(arr.ToList()).ToList();
                List<_Movie> favourites = (List<_Movie>)((await GetFavouriteMovies(token))?.data);

                foreach (_Movie fav in favourites)
                {
                    foreach(_Movie movie in _arr)
                    {
                        if (movie.id == fav.id) movie.isFavourite = true;
                    }
                }

                if (arr.Length > 0)
                {
                    resp.statusCode = 200;
                    resp.data = _arr;
                }
                else resp.statusCode = 404;
                return resp;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
