﻿using System;
namespace movieAPI.Models
{
    public class FavouriteMovie
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int MovieId { get; set; }
        public DateTime CreatedAt { get; set; }
        public User User { get; set; }
    }
}
