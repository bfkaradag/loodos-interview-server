﻿using System;
using System.Collections.Generic;
 using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace movieAPI.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool EmailVerified { get; set; }
        public string Password { get; set; }
        public DateTime? CreatedAt { get; set; }
        public List<FavouriteMovie> FavouriteMovies { get; set; }
    }
    public class Login
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
