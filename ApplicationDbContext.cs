﻿using System;
using Microsoft.EntityFrameworkCore;
using movieAPI.Models;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    { }
    public DbSet<User> Users { get; set; }
    public DbSet<FavouriteMovie> FavouriteMovies { get; set; }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<FavouriteMovie>()
            .HasOne(fm => fm.User)
            .WithMany(u => u.FavouriteMovies)
            .HasForeignKey(fm => fm.UserId);
    }

   
}