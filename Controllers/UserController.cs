﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using movieAPI.Models;
using movieAPI.Services;

namespace movieAPI.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;

        public UserController(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        
        [AllowAnonymous]
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] User user)
        {
            Response resp = await new UserService(_context, _configuration).Create(user);
            if (resp.statusCode == 200) return Ok(resp);
            if (resp.statusCode == 409) return Conflict(resp);
            return NotFound(resp);
        }

    }
}
