﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using movieAPI.Models;
using movieAPI.Services;


namespace movieAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class MovieController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;

        public MovieController(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }
        //[HttpPost("create")]
        //public async Task<IActionResult> Create([FromBody] FavouriteMovie movie)
        //{
        //    string token = Request.Headers["Authorization"];
        //    Response resp = await new MovieService(_context, _configuration).Movie(movie.MovieId, token);
        //    if (resp.statusCode == 200) return Ok(resp);
        //    return NotFound(resp);
        //}
        [HttpGet("favourites")]
        public async Task<IActionResult> GetFavourites()
        {
            string token = Request.Headers["Authorization"];
            Response resp = await new MovieService(_context, _configuration).GetFavouriteMovies(token);
            if (resp.statusCode == 200) return Ok(resp);
            return NotFound(resp);
        }
        [HttpGet("explore/{id}")]
        public async Task<ActionResult> ExploreMovies(int    id)
        {
            string token = Request.Headers["Authorization"];
            Response resp = await new MovieService(_context, _configuration).Explore(id, token);
            if (resp.statusCode == 200) return Ok(resp);
            return NotFound(resp);
        }
        [HttpGet("search")]
        public async Task<ActionResult> SearchMovies([FromQuery] string query)
        {
            string token = Request.Headers["Authorization"];
            Response resp = await new MovieService(_context, _configuration).Search(query, token);
            if (resp.statusCode == 200) return Ok(resp);
            return NotFound(resp);
        }
        [HttpGet("favourite/{movieId}")]
        public async Task<ActionResult> Favourite(int movieId)
        {
            string token = Request.Headers["Authorization"];
            Response resp = await new MovieService(_context, _configuration).Favourite(movieId, token);
            if (resp.statusCode == 200) return Ok(resp);
            return NotFound(resp);
        }
    }
}
