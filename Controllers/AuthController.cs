﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using movieAPI.Models;
using movieAPI.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace movieAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;

        public AuthController(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Authenticate([FromBody] Login login)
            {
           Response resp = await new AuthService(_context, _configuration).login(login);
            if (resp.statusCode == 200)
            {
                return Ok(resp);
            }
            else if (resp.statusCode == 403) return Forbid();
           return NotFound(resp);
        }
        [HttpGet("check")]
        public async Task<IActionResult> Check()
        {
            return Ok();
        }
        

    }
}
